# Lab 5 - Software Quality and Reliability

## 1. Parameters to be tested

   | Parameter          | Equivalence Class                 |
   | ------------------ | --------------------------------- |
   | type               | budget; luxury; nonsense          |
   | plan               | minute; fixed_price; nonsense     |
   | distance           | <=0; >0                           |
   | planned_distance   | <=0; >0                           |
   | time               | <=0; >0                           |
   | planned_time       | <=0; >0                           |
   | inno_discount      | yes; no; nonsense                 |

## 2. Decision Table

   | Parameters          | Values                            |  R1   |  R2   |  R3   |  R4   |    R5    |    R6    |    R7    |      R8       |    R9    |   R10    |      R11      |      R12      |   R13    |   R14    |
   | ------------------- | --------------------------------- | :---: | :---: | :---: | :---: | :------: | :------: | :------: | :-----------: | :------: | :------: | :-----------: | :-----------: | :------: | :------: |
   | distance            | <=0; >0; >10000                   | <=0   | >0    | >0    |  >0   |   >0     |   >0     |   >0     |     >0        |   >0     |   >0     |     >0        |     >0        |   >0     |   >0     |
   | planned_distance    | <=0; >0; >10000                   |   *   | <=0   | >0    |  >0   |   >0     |   >0     |   >0     |     >0        |   >0     |   >0     |     >0        |     >0        |   >0     |   >0     |
   | time                | <=0; >0; >10000                   |   *   |   *   | <=0   |  >0   |   >0     |   >0     |   >0     |     >0        |   >0     |   >0     |     >0        |     >0        |   >0     |   >0     |
   | planned_time        | <=0; >0; >10000                   |   *   |   *   |   *   |  <=0  |   >0     |   >0     |   >0     |     >0        |   >0     |   >0     |     >0        |     >0        |   >0     |   >0     |
   | type                | luxury, budget, nonsense          |   *   |   *   |   *   |   *   | nonsense |    *     |    *     |   luxury      |   budget |   budget |     budget    |     budget    |   luxury |   luxury |
   | plan                | fixed_price, minute, nonsense     |   *   |   *   |   *   |   *   |    *     | nonsense |    *     |   fixed_price |   minute |   minute |   fixed_price |   fixed_price |   minute |   minute |
   | inno_discount       | yes, no, nonsense                 |   *   |   *   |   *   |   *   |    *     |    *     | nonsense |       *       |     no   |    yes   |       no      |       yes     |     no   |    yes   |
   |                     |                                   |       |       |       |       |          |          |          |               |          |          |               |               |          |          |
   | Request status      |                                   |   ❌  |   ❌  |   ❌  |   ❌  |   ❌     |   ❌     |   ❌     |      ❌       |     ✅   |     ✅   |       ✅     |        ✅     |     ✅   |     ✅   |


   | Parameters          | Values                            |  R15   |  R16   |  R17   |  R18   |
   | ------------------- | --------------------------------- | :----: | :----: | :----: | :----: | 
   | distance            | <=0; >0; >10000                   | >10000 |    *   |    *   |   *    |
   | planned_distance    | <=0; >0; >10000                   |    *   | >10000 |    *   |   *    |
   | time                | <=0; >0; >10000                   |    *   |    *   | >10000 |   *    |
   | planned_time        | <=0; >0; >10000                   |    *   |    *   |    *   | >10000 |
   | type                | luxury, budget, nonsense          |    *   |    *   |    *   |   *    |
   | plan                | fixed_price, minute, nonsense     |    *   |    *   |    *   |   *    |
   | inno_discount       | yes, no, nonsense                 |    *   |    *   |    *   |   *    |
   |                     |                                   |        |        |        |        |
   | Request status      |                                   |   ❌   |   ❌   |   ❌  |   ❌    |

Where:
  - ❌: Invalid Request
  - ✅: Valid Request

## 3. My InnoCar specs

My email: `i.mpore@innopolis.university`

   ```bash
    Here is InnoCar Specs:
    Budget car price per minute = 25
    Luxury car price per minute = 65
    Fixed price per km = 12
    Allowed deviations in % = 6
    Inno discount in % = 12
   ```

## 4. Test Cases

| ID | Decision Table Entry |   `type`   |    `plan`     | `distance` | `planned_distance` | `time` | `planned_time` | `inno_discount` | Expected Result |  Actual Result  | Decision |
|----|----------------------|:----------:|:-------------:|:----------:|:------------------:|:------:|:--------------:|:---------------:|:---------------:|:---------------:|:--------:|
| 1  | R1                   | `nonsense` |   `minute`    |     1      |         1          |   1    |       1        |      `yes`      | Invalid Request | Invalid Request |     ✅   |
| 2  | R1                   |  `budget`  |  `nonsense`   |     1      |         1          |   1    |       1        |      `yes`      | Invalid Request | Invalid Request |     ✅   |
| 3  | R2                   |  `budget`  |   `minute`    |     1      |         1          |   1    |       1        |   `nonsense`    | Invalid Request | Invalid Request |     ✅   |
| 4  | R2                   |  `budget`  |   `minute`    |     -1     |         1          |   1    |       1        |     `yes`       | Invalid Request |      32.5       |     ❌   |
| 5  | R3                   |  `budget`  |   `minute `   |   100001   |         1          |   1    |       1        |       `yes`     | Invalid Request |      32.5       |     ❌   |
| 6  | R3                   |  `budget`  |   `minute`    |     1      |         -1         |   1    |       1        |     `yes`       | Invalid Request |      32.5       |     ❌   |
| 7  | R4                   |  `budget`  |   `minute`    |     1      |       100001       |   1    |       1        |      `yes`      | Invalid Request |      32.5       |     ❌   |
| 8  | R4                   |  `budget`  |   `minute`    |     1      |         1          |   -1   |       1        |      `yes`      | Invalid Request | Invalid Request |     ✅   |
| 9  | R5                   |  `budget`  |   `minute`    |     1      |         1          | 100001 |       1        |      `yes`      | Invalid Request |   32500032.5    |     ❌   |
| 10 | R5                   |  `budget`  |   `minute`    |     1      |         1          |   1    |       -1       |      `yes`      | Invalid Request | Invalid Request |     ✅   |
| 11 | R6                   |  `budget`  |   `minute`    |     1      |         1          |   1    |     100001     |      `yes`      | Invalid Request |      32.5       |     ❌   |
| 12 | R7                   |  `budget`  |   `minute`    |     0      |         1          |   1    |       1        |      `yes`      |       22.0      |      32.5       |     ❌   |
| 13 | R8                   |  `budget`  |   `minute`    |   100000   |         1          |   1    |       1        |      `yes`      |       22.0      |      32.5       |     ❌   |
| 14 | R8                   |  `budget`  |   `minute`    |     1      |         0          |   1    |       1        |      `yes`      |       22.0      |      32.5       |     ❌   |
| 15 | R9                   |  `budget`  |   `minute`    |     1      |       100000       |   1    |       1        |      `yes`      |       22.0      |      32.5       |     ❌   |
| 16 | R10                  |  `budget`  |   `minute`    |     1      |         1          |   0    |       1        |      `yes`      |       0.0       |        0        |     ✅   |
| 17 | R10                  |  `budget`  |   `minute`    |     1      |         1          | 100000 |       1        |      `yes`      |   22000000.0    |     32500000    |     ❌   |
| 18 | R10                  |  `budget`  |   `minute`    |     1      |         1          |   1    |       0        |      `yes`      |       22.0      |      32.5       |     ❌   |
| 19 | R10                  |  `budget`  |   `minute`    |     1      |         1          |   1    |     100000     |      `yes`      |       22.0      |      32.5       |     ❌   |
| 20 | R11                  |  `budget`  |   `minute`    |     50     |        50          |   50   |       50       |      `yes`      |      1100.0     |      1625       |     ❌   |
| 21 | R11                  |  `budget`  |   `minute`    |     50     |        50          |   50   |       50       |      `no`       |       1250      |      1625       |     ❌   |
| 22 | R11                  |  `budget`  | `fixed_price` |     50     |        50          |   50   |       50       |      `yes`      |       528.0     |      625        |     ❌   |
| 23 | R11                  |  `budget`  | `fixed_price` |     50     |        50          |   50   |       50       |      `no`       |       600       |      625        |     ❌   |
| 24 | R12                  |  `luxury`  |   `minute`    |     50     |        50          |   50   |       50       |      `yes`      |       2860      |      3250       |     ❌   |
| 25 | R13                  |  `luxury`  |   `minute`    |     50     |        50          |   50   |       50       |      `no`       |       3250      |      3250       |     ✅   |
| 26 | R13                  |  `luxury`  | `fixed_price` |     50     |        50          |   50   |       50       |      `yes`      | Invalid Request | Invalid Request |     ✅   |
| 27 | R14                  |  `luxury`  | `fixed_price` |     50     |        50          |   50   |       50       |      `no`       | Invalid Request | Invalid Request |     ✅   |

- `Computed tests`: 27 
- `Passed tests`: 9 
- `Failed tests`: 18

## 5. Found Bugs

   - When `distance` is negative, we're getting a valid response and it's supposed to be invalid
   - When `distance` is greater than maximum `100000`, we're getting a valid response and it's supposed to be invalid
   - When `planned_distance` negative, we're getting a valid response and it's supposed to be invalid
   - When the `planned_distance` is greater than maximum `100000`, we're getting a valid response and it's supposed to be invalid
   - When the `time` is greater than maximum `100000`, we're getting a valid response and it's supposed to be invalid
   - When the `planned_time` is greater than maximum `100000`, we're getting a valid response and it's supposed to be invalid
   - There are some errors in system's calculations making the actual result differ from expected results.

